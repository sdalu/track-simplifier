# -*- encoding: utf-8 -*-
$:.unshift File.expand_path("../lib", __FILE__)
require "track-simplifier"

Gem::Specification.new do |s|
  s.name          = "track-simplifier"
  s.version       = TrackSimplifier::VERSION
  s.authors       = [ "Stephane D'Alu" ]
  s.email         = [ "stephane.dalu@gmail.com" ]
  s.homepage      = "http://github.com/sdalu/track-simplifier"
  s.summary       = "Simplify track"
  #s.description   = "Simplify track"

  s.add_development_dependency "rake"
  s.add_development_dependency "yard"
  s.add_development_dependency "redcarpet"
  s.add_development_dependency "github-markup"

  s.has_rdoc      = 'yard'

  s.license       = 'MIT'
  

  s.files         = %w[ LICENSE Gemfile track-simplifier.gemspec ] 	+ 
		     Dir['lib/**/*.rb'] 
  #s.require_path  = 'lib'
end
