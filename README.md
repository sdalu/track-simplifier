# track-simplifier

Simplify a polyline using various methods (radial distance,
Douglas Peucker, Reuman Witkam, ...).

* The polyline is simply defined as an array of points:
 `[ p1, p2, ..., p3 ]`

* Two methods need to be provided, one to compute distance
between point `a` to point `b` and the other for the distance between
point `p` and line (`a`,`b`).


Example:
```ruby
# The polyline
polyline   = [ {x: 1, y: 1}, {x: 5, y: 7}, ....., {x: 8, y: 1} ]

# The two methods to compute distance
# They also deal with the point structure (Array, Hash, ....)
dist_point = ->(a,b)    {   a,b =                [a[:x],a[:y]], [b[:x],b[:y]]
                            a,b =             Vector[*a], Vector[*b]
                 (b - a).norm
           }
dist_line  = ->(p, a,b) { p,a,b = [p[:x],p[:y]], [a[:x],a[:y]], [b[:x],b[:y]]
                          p,a,b = Vector[*p], Vector[*a], Vector[*b]
                 pa, ba = p-a, b-a
                 t      = pa.dot(ba)/ba.dot(ba)
                 (pa - t * ba).norm
           }

# Perform simplification
$ts = TrackSimplifier.new(point2point: dist_point, point2line: dist_line)
$ts.radial_distance(polyline, radius)    # Radial distance
$ts.reumann_witkam(polyline,  threshold) # Reumann Witkam
```


This work was supported by the [Privamov](http://liris.cnrs.fr/privamov/project/) project funded by LABEX IMU (ANR-10-LABX-0088) of Université de Lyon, within the program "Investissements d’Avenir" (ANR-11- IDEX-0007) operated by the French National Research Agency (ANR)
