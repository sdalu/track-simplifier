# coding: utf-8
require 'matrix'

# https://trac.osgeo.org/grass/browser/grass/trunk/vector/v.generalize/simplification.c

#
# Simplify a polyline using various methods (radial distance,
# Douglas Peucker, Reuman Witkam, ...).
#
# The polyline is simply defined as an array of points:
#  polyline = [ p1, p2, ..., p3 ]
#
# Two methods need to be provided, one to compute distance
# between point `a` to point `b` and the other for the distance between
# point `p` and line (`a`,`b`), and they also deal with the point
# structure (Array, Hash, ....)
#  dist_point = ->(a,b)    {   a,b =                [a[:x],a[:y]], [b[:x],b[:y]]
#                              a,b =                Vector[*a],    Vector[*b]
#                   (b - a).norm
#            }
#  dist_line  = ->(p, a,b) { p,a,b = [p[:x],p[:y]], [a[:x],a[:y]], [b[:x],b[:y]]
#                            p,a,b = Vector[*p],    Vector[*a],    Vector[*b]
#                   pa, ba = p-a, b-a
#                   t      = pa.dot(ba)/ba.dot(ba)
#                   (pa - t * ba).norm
#             }
# The value returned by these functions will be compared between them
# or with a threshold value, it means that they should implemenent
# the comparator mixin `Comparable`
#
# Perform simplification
#   $ts = TrackSimplifier.new(point2point: dist_point, point2line: dist_line)
#   $ts.radial_distance(polyline, radius)    # Radial distance
#   $ts.reumann_witkam(polyline,  threshold) # Reumann Witkam
#
class TrackSimplifier
    # Library version
    VERSION = "0.1.0"
    
    def initialize(point2point: nil, point2line: nil)
        @point2point = point2point
        @point2line  = point2line
    end

    # Helpers for euclidian distance (requires ruby >= 2.2)
    EUCLIDIAN = { 
        point2point: ->(a,b)    {   a,b =             Vector[*a], Vector[*b]
            (b - a).norm
        },
        point2line:  ->(p, a,b) { p,a,b = Vector[*p], Vector[*a], Vector[*b]
            pa, ba = p-a, b-a
            t      = pa.dot(ba)/ba.dot(ba)
            (pa - t * ba).norm
        }
    }


    # Simplify polyline using the Reuman Witkam method
    # {http://psimpl.sourceforge.net/reumann-witkam.html}
    # @param polyline [Array]
    # @return [Array]
    def reumann_witkam(polyline, thresh)
        return polyline if polyline.size <= 2

        track = [ polyline.first ]
        a, b  = polyline[0], polyline[1]
        
        for i in 2..(polyline.size-1) do
            if point2line(polyline[i], a,b) > thresh
                a, b = polyline[i-1], polyline[i]
                track.push(a)
            end
        end
        track.push(polyline.last)
    end


 
 
    # Simplify polyline using Lang method
    # @param polyline [Array]
    # @return [Array]
    def lang(polyline, thresh, look_ahead)
        return polyline if polyline.size <= 2

        first, last = 0, [look_ahead, polyline.size-1].min
        track       = [ polyline[first] ]

        while (first < polyline.size-1)
            a, b = polyline[first], polyline[last]
            
            if ((first+1)..(last-1)).any? {|i|
                   point2line(polyline[i], a,b) > thresh }
                last -= 1
            else
                track << polyline[last]
                first, last = last, [ last+look_ahead, polyline.size-1].min
            end
        end
        track
    end

    # Simplify polyline using Douglas Peucker method
    # @param polyline [Array]
    # @return [Array]
    def douglas_peucker(polyline, thresh)
        return polyline if polyline.size <= 2
        
        stack = [ 0, polyline.size-1 ]
        index = [ 0 ]

        while (!stack.empty?) do
            first, last       = stack.pop(2)
            a, b              = polyline[first], polyline[last]

            maxindex, maxdist = ((first+1)..(last-1))
                .map {|i| [ i, point2line(polyline[i], a,b) ] }
                .max {|a, b| a[1] <=> b[1] }

            if maxindex.nil? || maxdist <= thresh
            then index.push(last)
            else stack.push(maxindex, last).push(first, maxindex)
            end
        end

        index.map {|i| polyline[i] }
    end


    # Simplify polyline using radial distance method
    # {http://psimpl.sourceforge.net/radial-distance.html}
    #
    # When using a merger, returned value can be an array of point to
    # use instead of there reference point, +nil+ to use the reference
    # point or +false+ to cancel the simplification and adding all the
    # intermediate points back
    #
    # @note If a merger is given, simplification will skip the
    #       first point of the track to ensure that start point
    #       and end point are not altered
    # @param polyline [Array]
    # @param radius [Numeric]
    # @param merger [Proc] the merging process
    # @return [Array]
    def radial_distance(polyline, radius, merger: nil)
        if !merger.nil? && !merger.respond_to?(:call)
            raise ArgumentError, "merger is not call-able"
        end
        return polyline if polyline.size <= (merger.nil? ? 2 : 3)

        track = []
        start = 0;
        track << polyline[start    ]
        track << polyline[start = 1] if !merger.nil?

        for i in (merger.nil? ? 1 : 2)..(polyline.size - 2) do
            if point2point(polyline[i], polyline[start]) > radius
                if merger && (i - start) >= 2
                    case list = merger.call(polyline[start..i-1])
                    when nil   # act as if merger wasn't use
                    when false # put all the point back (cancel simplification)
                        track += polyline[start+1..i-1]
                    when Array # use returned list instead of reference point
                        track.pop  
                        track += list
                    else raise ArgumenError,
                               "unexpected return value for merger"
                    end
                end
                track << polyline[start = i]
	    end
        end
        track.push(polyline.last)
    end



    

    # Simplify polyline only keeping n-th point
    # {http://psimpl.sourceforge.net/nth-point.html}
    # @param polyline [Array]
    # @return [Array]
    def nth_point(polyline, skip: 1)
        _enum = polyline.each
        Enumerator.new {|y|
            nxt = nil
            begin
                y.yield(_enum.next)

                while true do # don't use 'loop' as it rescue StopIteration
                    nxt = nil ; skip.times { nxt = _enum.next }
                    y.yield(_enum.next)
                end
            rescue StopIteration
                y.yield(nxt) unless nxt.nil?
            end
        }
    end

    
    # (see #radial_distance)
    def radial_distance_enum(polyline, radius, merger: nil)
        if !merger.nil? && !merger.respond_to?(:call)
            raise ArgumentError, "merger is not call-able"
        end
        _enum = polyline.each
        Enumerator.new {|y|
            begin
                key = _enum.next

                while true do 
                    skp  = []
                    nxt = nil
                    while true do
                        nxt = _enum.next
                        break if point2point(key, nxt) >= radius
                        skp << nxt
                    end
                    
                    if skp.empty?
                        y << key
                    else
                        if merger
                            case list = merger.call([key] + skp)
                            when nil   # act as if merger wasn't use
                            when false # put all the point back
                                y << key
                                y.each {|val| y << val }
                                track += polyline[start+1..i-1]
                            when Array # use returned list instead
                                list.each {|val| y << val }
                            else raise ArgumenError,
                                       "unexpected return value for merger"
                            end
                        else
                            y << key
                        end
                    end

                    key = nxt
                end
            rescue StopIteration
                if key
                    if skp.empty?
                        y << key
                    else
                        if merger
                            case list = merger.call([key] + skp)
                            when nil   # act as if merger wasn't use
                            when false # put all the point back
                                y << key
                                y.each {|val| y << val }
                                track += polyline[start+1..i-1]
                            when Array # use returned list instead
                                list.each {|val| y << val }
                            else raise ArgumenError,
                                       "unexpected return value for merger"
                            end
                        else
                            y << key
                        end
                    end
                end
            end
        }
    end


    protected

    def point2point(a,b)
        @point2point.call(a,b)
    end

    def point2line(p, a,b)
        @point2line.call(p, a,b)
    end
    
end
